% input: f -> a 9-joint robot encoded as a SerialLink class
%        qInit -> 1x9 vector denoting current joint configuration
%        posGoal -> 3x1 vector denoting the target position to move to
% output: q -> 1x9 vector of joint angles that cause the end
%              effector position to reach <position>
%              (orientation is to be ignored)

function q = Q2(f, qInit, posGoal) 
    alpha = 0.1;
    q = qInit;
    T = f.fkine(q);
    x = T.t;
    while(norm(posGoal - x) > 0.01)

        dx = posGoal - x;
        J = f.jacob0(q);
        J_pseudo = pinv(J);
        dq = alpha * J_pseudo(:,1:3) * dx; %%taking the first 3 columns because we don't care about orientation
        q = q(:) + dq;
        
        T = f.fkine(q);
        x = T.t;
    end
end