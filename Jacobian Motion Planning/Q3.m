% input: f -> a 9-joint robot encoded as a SerialLink class
%        qInit -> 1x9 vector denoting current joint configuration
%        posGoal -> 3x1 vector describing goal position
%        epsilon -> scalar denoting how close end effector must be before
%                   loop terminates
%        velocity -> scalar denoting desired velocity of end effector
% output: traj -> nx9 matrix that denotes arm trajectory. Each row denotes
%                 a joint configuration. The first row is the first
%                 configuration. The last is the last configuration in the
%                 trajectory.

function traj = Q3(f, qInit, posGoal, epsilon, velocity)
    i = 1;
    q = qInit;
    T = f.fkine(q);
    x = T.t;
    while(norm(posGoal - x) >= epsilon)

        dx = (posGoal - x);
        dx = dx/norm(dx);
        J = f.jacob0(q);
        J_pseudo = pinv(J);
        dq = J_pseudo(:,1:3) * dx  * velocity; %%taking the first 3 rows because we don't care about orientation
        q = q(:) + dq;
        
        T = f.fkine(q);
        x = T.t;
        traj(i,:) = q;
        i = i + 1;
    end
    
    %velocity check
    for i=1:size(traj,1)-1
        actual_velocity = norm(f.fkine(traj(i+1,:)) - f.fkine(traj(i,:)));
        if((abs(actual_velocity - velocity)) < 0.001)
            continue
        else
            disp("Velocity doesn't match requirement!")
            break
    end
end