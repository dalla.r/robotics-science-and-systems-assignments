% input: f -> a 9-joint robot encoded as a SerialLink class
%        qInit -> 1x9 vector denoting current joint configuration
%        circle -> 3xn matrix of Cartesian positions that describe the
%                  circle
%        velocity -> scalar denoting desired velocity of end effector
% output: traj -> nx9 matrix that denotes arm trajectory. Each row denotes
%                 a joint configuration. The first row is the first
%                 configuration. The last is the last configuration in the
%                 trajectory. The end effector should trace a circle in the
%                 workspace, as specified by the input argument circle.
%                 (orientation is to be ignored)

function traj = Q4(f, qInit, circle, velocity)

    traj = [];
    for i = 2:size(circle,2)
        traj_temp = Q3(f, qInit, circle(:,i), 0.05, velocity);
        qInit = traj_temp(end,:);
        x = size(traj,1);
        traj(x+1:size(traj_temp,1)+x,:) = traj_temp;
    end
    
%     %velocity check commented because already performed in Q3
%     for i=1:size(traj,1)-1
%         actual_velocity = norm(f.fkine(traj(i+1,:)) - f.fkine(traj(i,:)));
%         if((abs(actual_velocity - velocity)) < 0.001)
%             continue
%         else
%             disp("Velocity doesn't match requirement!")
%             break
%     end
end