% input: f1 -> an 9-joint robot encoded as a SerialLink class for one
%              finger
%        f2 -> an 9-joint robot encoded as a SerialLink class for one
%              finger
%        qInit -> 1x11 vector denoting current joint configuration.
%                 First seven joints are the arm joints. Joints 8,9 are
%                 finger joints for f1. Joints 10,11 are finger joints
%                 for f2.
%        f1Target, f2Target -> 3x1 vectors denoting the target positions
%                              each of the two fingers.
% output: q -> 1x11 vector of joint angles that cause the fingers to
%              reach the desired positions simultaneously.
%              (orientation is to be ignored)

function q = Q5(f1, f2, qInit, f1Target, f2Target)
    
    alpha = 0.1;
    q = qInit;
    [T1 T2] = two_finger_fkine(f1,f2,q);
    x1 = T1.t;
    x2 = T2.t;

    while(norm(f1Target - x1) > 0.01 && norm(f2Target - x2) > 0.01)

        dx = [f1Target - x1;
              f2Target - x2];
        J = two_finger_linear_jacobian(f1,f2,q);
        J_pseudo = pinv(J);
        dq = alpha * J_pseudo * dx;
        q = q(:) + dq;
        [T1 T2] = two_finger_fkine(f1,f2,q);
        x1 = T1.t;
        x2 = T2.t;
    end
    
end

function [T1 T2] = two_finger_fkine(f1,f2,q)
    T1 = f1.fkine(q(1:9));
    T2 = f2.fkine(q_2(q));
end

function J_L = two_finger_linear_jacobian(f1,f2,q)
    J1 = jacob0(f1,q(1:9));
    J2 = jacob0(f2,q_2(q));
    J1_L = J1(1:3,:);
    J2_L = J2(1:3,:);
    J_L = [J1_L(:,1:9) zeros(3,2);
           J2_L(:,1:7) zeros(3,2) J2_L(:,8:9)];
end

function q2 = q_2(q)
    q2 = [q(1) q(2) q(3) q(4) q(5) q(6) q(7) q(10) q(11)];
end