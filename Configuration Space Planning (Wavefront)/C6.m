% Input: robot -> A 2-DOF robot encapsulated in a MATLAB cell with fields:
%                 robot.link1, robot.link2, robot.pivot1, robot.pivot2
%                 See description in hw2_cspace and demo code in C1.
%        obstacles -> 1xN vector of polyshape objects describing N 2-D
%                     polygonal obstacles
%        q_path -> Mx2 matrix containing a collision-free path from
%                  q_start to q_goal. Each row in q_path is a robot
%                  configuration. The first row should be q_start,
%                  the final row should be q_goal.
% Output: num_collisions -> Number of swept-volume collisions encountered
%                           between consecutive configurations in q_path

function num_collisions = C6(robot, obstacles, q_path)

    num_collisions = 0;
    [old_poly1, old_poly2, old_pivot1, old_pivot2] = q2poly(robot, q_path(1,:));
    
    for i = 2:length(q_path)
        [poly1, poly2, pivot1, pivot2] = q2poly(robot, q_path(i,:));
        polyout1 = union(poly1, old_poly1);
        polyout2 = union(poly2, old_poly2);
        for(k = 1:length(obstacles))
            if not(isempty(intersect(convhull(polyout1), obstacles(k)).Vertices) && isempty(intersect(convhull(polyout2),obstacles(k)).Vertices))
                num_collisions = num_collisions + 1;
                plot(convhull(polyout1));
                plot(convhull(polyout2));
            end
        end
        [old_poly1, old_poly2, old_pivot1, old_pivot2] = q2poly(robot, q_path(i,:));
    end
end