% Input: distances -> NxN matrix containing the distance transform from
%                      the goal configuration
%                      == 0 if cell is unreachable
%                      == 1 if cell is an obstacle
%                      == 2 if cell is the goal
%                      >  2 otherwise
%        q_grid -> 1xN vector of angles between 0 and 2*pi, discretizing
%                  each dimension of configuration space
%        q_start -> 2x1 vector denoting the start configuration
% Output: path -> Mx2 matrix containing a collision-free path from q_start
%                 to q_goal (as computed in C3, embedded in distances).
%                 The entries of path should be grid cell indices, i.e.,
%                 integers between 1 and N. The first row should be the
%                 grid cell containing q_start, the final row should be
%                 the grid cell containing q_goal.

function path = C4(distances, q_grid, q_start)
    
    ids = [-1 0 1];
    % Find q_grid closest point to q_start
    [q1, idx1] = min(abs(q_grid - q_start(1,:)));
    [q2, idx2] = min(abs(q_grid - q_start(2,:)));
    start_idx = [idx1, idx2];
    
    d = distances(idx1, idx2);
    visited = zeros(size(distances));
    path = [start_idx];
    previous_d = 1;
    
    while not(previous_d == 2)        

        idx = path(end,:);
        previous_d = distances(idx(:,1), idx(:,2));
        
        % Going through each neighbor
        for i = 1:length(ids)
            for j = 1:length(ids)
                if i==0 && j==0
                    continue
                else
                    n_i = idx(:,1) + ids(i);
                    n_j = idx(:,2) + ids(j);
                    
                    n_i = bound(n_i, distances);
                    n_j = bound(n_j, distances);
                    
                    d = distances(n_i, n_j);
                    
                    if d < previous_d && visited(n_i, n_j) == 0 && not(d == 1)
                        path(end+1,:) = [n_i, n_j];
                        visited(n_i, n_j) = 1;
                        break
                    end
                end
            end
        end
    end
       
end


function n = bound(n, limit)
    n = max(1, n);
    n = min(length(limit), n);
end

    