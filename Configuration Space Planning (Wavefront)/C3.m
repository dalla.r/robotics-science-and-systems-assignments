% Input: cspace -> NxN matrix: cspace(i,j)
%                   == 1 if [q_grid(i); q_grid(j)] is in collision,
%                   == 0 otherwise
%        q_grid -> 1xN vector of angles between 0 and 2*pi, discretizing
%                  each dimension of configuration space
%        q_goal -> 2x1 vector denoting the goal configuration
% Output: distances -> NxN matrix containing the distance transform from
%                      the goal configuration
%                      == 0 if cell is unreachable
%                      == 1 if cell is an obstacle
%                      == 2 if cell is the goal
%                      >  2 otherwise

function distances = C3(cspace, q_grid, q_goal)

    ids = [-1 0 1];
    % Find q_grid closest point to q_goal
    [q1, idx1] = min(abs(q_grid - q_goal(1,:)));
    [q2, idx2] = min(abs(q_grid - q_goal(2,:)));
    start_idx = [idx1, idx2];

    L = [start_idx];
    d = zeros(size(cspace));    
    % Initialize distances matrix d
    for i = 1:length(cspace)
        for j = 1:length(cspace)
            if cspace(i, j) == 1
                d(i,j) = cspace(i,j);
            end
        end
    end
    d(start_idx(:,1), start_idx(:,2)) = 2;

    % loop until stack L is empty
    while not(isempty(L))
        
        idx = L(1,:);
        L(1,:) = [];
        current_d = d(idx(:,1), idx(:,2));
        % Going through each neighbor
        for i = 1:length(ids)
            for j = 1:length(ids)
                if i==0 && j==0
                    continue
                else
                    n_i = idx(:,1) + ids(i);
                    n_j = idx(:,2) + ids(j);

                    % Making sure the neighbor's index is not <1 or >cspace
                    % length
                    n_i = bound(n_i, cspace);
                    n_j = bound(n_j, cspace);

                    if d(n_i, n_j) == 0 
                        d(n_i, n_j) = current_d + 1;
                        L = [L; [n_i, n_j]];
                    end
                end
            end
        end
    end
    distances = d;
end

function n = bound(n, limit)
    n = max(1, n);
    n = min(length(limit), n);
end

    
   