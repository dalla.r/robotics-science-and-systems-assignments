% Input: robot -> A 2-DOF robot encapsulated in a MATLAB cell with fields:
%                 robot.link1, robot.link2, robot.pivot1, robot.pivot2
%                 See description in hw2_cspace and demo code in C1.
%        q -> 2x1 vector denoting the configuration to convert to polygons
% Output: poly1 -> A polyshape object denoting the 2-D polygon describing
%                  link 1
%         poly2 -> A polyshape object denoting the 2-D polygon describing
%                  link 2
%         pivot1 -> 2x1 vector denoting the 2-D position of the pivot point
%                   of link 1 (frame 1 origin), with respect to base frame
%         pivot2 -> 2x1 vector denoting the 2-D position of the pivot point
%                   of link 2 (frame 2 origin), with respect to base frame

function [poly1, poly2, pivot1, pivot2] = q2poly(robot, q)
    
    R1 = rot2(q(1));
    R2 = rot2(q(1) + q(2));
    
    pivot1_temp = robot.pivot1;
    pivot2_temp = R1 * robot.pivot2;
    poly1_points = R1 * robot.link1;
    poly2_points = R2 * robot.link2;
    
    % Translate frame origins
    pivot1 = pivot1_temp;
    pivot2 = pivot1_temp + pivot2_temp;
    % Compute link polygon corners
    poly1_points_at0 = poly1_points + pivot1;
    poly2_points_at0 = poly2_points + pivot2;
    
    poly1 = polyshape(poly1_points_at0(1,:), poly1_points_at0(2,:));
    poly2 = polyshape(poly2_points_at0(1,:), poly2_points_at0(2,:));
    
end