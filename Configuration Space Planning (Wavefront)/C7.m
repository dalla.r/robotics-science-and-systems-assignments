% Input: cspace -> NxN matrix: cspace(i,j)
%                  == 1 if [q_grid(i); q_grid(j)] is in collision,
%                  == 0 otherwise
% Output: padded_cspace -> NxN matrix: padded_cspace(i,j)
%                          == 1 if cspace(i,j) == 1, or some neighbor of
%                                  cell (i,j) has value 1 in cspace
%                                  (including diagonal neighbors)
%                          == 0 otherwise

function padded_cspace = C7(cspace)

    ids = [-1 0 1];
    padded_cspace = zeros(size(cspace));
    visited = zeros(size(cspace));
    
    for i = 1:length(cspace)
        for j = 1:length(cspace)
            if cspace(i,j)
                 for m = 1:length(ids)
                    for n = 1:length(ids)
                        if m==0 && n==0
                            continue
                        else
                            n_m = bound(i + ids(m), cspace);
                            n_n = bound(j + ids(n), cspace);
                            
                            if not(visited(n_m, n_n)  || cspace(n_m, n_n))
                                padded_cspace(n_m, n_n) = 1;
                            end
                        end
                    end
                 end
            end
        end
    end
        
end

function n = bound(n, limit)
    n = max(1, n);
    n = min(length(limit), n);
end
