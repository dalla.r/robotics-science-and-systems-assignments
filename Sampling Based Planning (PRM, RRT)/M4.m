% Input: robot -> A 4-DOF robot encoded as a SerialLink object
%        q_min -> 1x4 vector of minimum angle for each joint
%        q_max -> 1x4 vector of maximum angle for each joint
%        q_start -> 1x4 vector denoting the start configuration
%        q_goal -> 1x4 vector denoting the goal configuration
%        link_radius -> Scalar denoting radius of each robot link's
%                       cylindrical body
%        sphere_centers -> Nx3 matrix containing the centers of N spherical
%                          obstacles
%        sphere_radii -> Nx1 vector containing the radii of N spherical
%                        obstacles
% Output: path -> Nx4 matrix containing a collision-free path between
%                 q_start and q_goal, if a path is found. The first row
%                 should be q_start, the final row should be q_goal.
%         path_found -> Boolean denoting whether a path was found

function [path, path_found] = M4(robot, q_min, q_max, q_start, q_goal, link_radius, sphere_centers, sphere_radii)
    step = 0.3;
    N = 1000;
    beta = 0.1;
    V = q_start;
    E = zeros(N);
    for(i = 0:N)
        if(rand(0,1) < beta) q_target = q_goal;
        else q_target =  M1(q_min, q_max, 1);
        end
        for(i = 1:size(V,1))
            distance(i) = norm(V(i,:)-q_target);
        end
        [~, idx_near] = min(abs(distance));
        q_near = V(idx_near,:);
        q_new = q_near + step/(norm(q_target - q_near)) * (q_target - q_near);
        if(~check_collision(robot, q_new, link_radius, sphere_centers, sphere_radii) && ~check_edge(robot, q_near, q_new, link_radius, sphere_centers, sphere_radii))
            V = [V; q_new];
            E(idx_near,size(V,1)) = norm(q_new - q_near);
        end
    end
    
    for(i = 1:size(V,1))
        distance(i) = norm(V(i,:)-q_goal);
    end
    [~, idx_goal] = min(abs(distance(:)));
    [path path_length] = shortestpath(digraph(E), 1, idx_goal);
    path = V(path,:);
    if(isinf(path_length)) path_found = 0;
    else path_found = 1;
end





