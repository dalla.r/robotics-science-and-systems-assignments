% Input: robot -> A 4-DOF robot encoded as a SerialLink object
%        q_min -> 1x4 vector of minimum angle for each joint
%        q_max -> 1x4 vector of maximum angle for each joint
%        num_samples -> Integer denoting number of samples in PRM
%        num_neighbors -> Integer denoting number of closest neighbors to
%                         consider in PRM
%        link_radius -> Scalar denoting radius of each robot link's
%                       cylindrical body
%        sphere_centers -> Nx3 matrix containing the centers of N spherical
%                          obstacles
%        sphere_radii -> Nx1 vector containing the radii of N spherical
%                        obstacles
% Output: samples -> num_samples x 4 matrix, sampled configurations in the
%                    roadmap (vertices)
%         adjacency -> num_samples x num_samples matrix, the weighted
%                      adjacency matrix denoting edges between roadmap
%                      vertices. adjacency(i,j) == 0 if there is no edge
%                      between vertex i and j; otherwise, its value is the
%                      weight (distance) between the two vertices. For an
%                      undirected graph, the adjacency matrix should be
%                      symmetric: adjacency(i,j) == adjacency(j,i)

function [samples, adjacency] = M2(robot, q_min, q_max, num_samples, num_neighbors, link_radius, sphere_centers, sphere_radii)
    samples = [];
    adjacency = zeros(num_samples);
    while(size(samples,1) < num_samples)
        q = M1(q_min, q_max, 1);
        if(~check_collision(robot, q, link_radius, sphere_centers, sphere_radii))
            samples = [samples; q];
        end    
    end
    distance = 0;
    for(i = 1:size(samples,1))
        neighbors = 0;
        for(n = 1:size(samples,1))  %Compute distance between configurations
            distance(n) = norm(samples(i)-samples(n));
        end
        neighbors = find_n_min(distance, num_neighbors);  %Find n neighbors indeces
        for(j_n = 1:length(neighbors))
            if(~check_edge(robot, samples(i,:), samples(neighbors(j_n),:), link_radius, sphere_centers, sphere_radii))
                adjacency(i,neighbors(j_n)) = distance(neighbors(j_n));
                adjacency(neighbors(j_n),i) = distance(neighbors(j_n));
            end
        end
    end  
end


function neighbors = find_n_min(distances, n)
    val = 0;
    idx = 0;
    for(i = 1:n+1) %find n minimum terms not including the smallest (would be 0)
        [val(i), idx(i)] = min(distances);
        distances(idx(i)) = [];
    end
    neighbors = idx(2:n+1);
end


