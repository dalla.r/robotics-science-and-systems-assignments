% Input: robot -> A 4-DOF robot encoded as a SerialLink object
%        samples -> num_samples x 4 matrix, vertices in the roadmap
%        adjacency -> num_samples x num_samples matrix, the weighted
%                     adjacency matrix denoting edges in the roadmap
%        q_start -> 1x4 vector denoting the start configuration
%        q_goal -> 1x4 vector denoting the goal configuration
%        link_radius -> Scalar denoting radius of each robot link's
%                       cylindrical body
%        sphere_centers -> Nx3 matrix containing the centers of N spherical
%                          obstacles
%        sphere_radii -> Nx1 vector containing the radii of N spherical
%                        obstacles
% Output: path -> Nx4 matrix containing a collision-free path between
%                 q_start and q_goal, if a path is found. The first row
%                 should be q_start, the final row should be q_goal.
%         path_found -> Boolean denoting whether a path was found

function [path, path_found] = M3(robot, samples, adjacency, q_start, q_goal, link_radius, sphere_centers, sphere_radii)
    for(i = 1:size(samples,1))
        distance(i,1) = norm(samples(i,:)-q_start);
        distance(i,2) = norm(samples(i,:)-q_goal);
    end
    [~, idx_on] = min(abs(distance(:,1)));
    [~, idx_off] = min(abs(distance(:,2)));

    [path path_length] = shortestpath(digraph(adjacency),idx_on, idx_off);
    path = samples(path,:);
    if(isinf(path_length)) path_found = 0;
    else path_found = 1;
end


