% Localize a sphere in the point cloud. Given a point cloud as input, this
% function should locate the position and radius of a sphere.
% input: ptCloud -> a pointCloud object that contains the point cloud (see
%                   Matlab documentation)
% output: center -> 3x1 vector denoting sphere center
%         radius -> scalar radius of sphere
function [center,radius] = Q2(ptCloud)
    P_orig = ptCloud.Location';

    P_shuffle = P_orig(:,randperm(length(P_orig)));
    P = P_shuffle(:,1:round(0.005*length(P_shuffle)):end);
    ptCloud = pointCloud(P');
    
    normals = pcnormals(ptCloud)';
    inliers_threshold = 0.003; % threshold to consider a point an inlier
    inliers_count = 0;
    inliers_req = 20000; % unmber of inliers that cause loop to break
    flag = 0;
    r_num = 10; % number of radii to sample
    r = linspace(0.05,0.11,r_num);
    k = 1;
    
    for i = 1:length(P)
        p = P(:,i);
        n = normals(:,i);
        
        for j = 1:length(r)
            dp = r(j)/sqrt(n(1)^2+n(2)^2+n(3)^2);
            c(:,i,j) = p + dp * n;
            error = abs(pdist2(c(:,i,j)', P_orig', 'Euclidean') - r(j));
            inliers_count(k) = length(error(error < inliers_threshold)); %compute number of inliers from error
            
            if inliers_count(k) > inliers_req %break if reached desired number of inliers
                flag = 1;
                break;
            end
            k = k + 1;
        end
        if flag break;
        end
    end
    
    [max_inliers, idx] = max(inliers_count);
    if mod(idx,r_num) == 0
        idx_r = 1;
    else
        idx_r = mod(idx,r_num);
    end
    
    [max_inliers, idx_i] = max(inliers_count);
    idx = ceil(idx_i/r_num);
    idx_r = mod(idx_i,r_num);
    if idx_r == 0 idx_r = r_num;
    end
    
    center = c(:,idx, idx_r);
    radius = r(idx_r);
end
