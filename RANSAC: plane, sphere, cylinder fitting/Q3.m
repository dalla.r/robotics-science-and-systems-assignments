% Localize a cylinder in the point cloud. Given a point cloud as input, this
% function should locate the position and orientation, and radius of the
% cylinder.
% input: ptCloud -> a pointCloud object that contains the point cloud (see
%                   Matlab documentation)
% output: center -> 3x1 vector denoting cylinder center
%         axis -> 3x1 unit vector pointing along cylinder axis
%         radius -> scalar radius of cylinder
function [center,axis,radius] = Q3(ptCloud)
    P_orig = ptCloud.Location';
    P_shuffle = P_orig(:,randperm(length(P_orig)));
    P = P_shuffle(:,1:10:end);
    ptCloud = pointCloud(P'); %shuffled point cloud
    normals = pcnormals(ptCloud)'; %shuffled normals
    inliers_threshold = 0.001; % threshold to consider a point an inlier
    inliers_count = 0;
    inliers_req = 20000; % number of inliers that cause loop to break
    flag = 0;
    r_num = 5; % number of radii to sample
    r = linspace(0.05,0.1,r_num);
    k = 1; %inlier iterator
    
    for i = 2:length(P)
        p(:,1:2) = P(:,i-1:i); %sample 2 points
        n = normals(:,i-1:i); %2 surface normals
        ax(:,i-1) = cross(n(:,1),n(:,2)); %candidate axis    

        for j = 1:length(r)
            dp = r(j)/sqrt(n(1,1)^2+n(2,1)^2+n(3,1)^2);
            c(:,i-1,j) = p(:,1) + dp * n(:,1); %candidate center from 1 of the sampled points
            
            P_proj = (eye(3) - ax(:,i-1) * ax(:,i-1)') * P_orig; 
            c_proj = (eye(3) - ax(:,i-1) * ax(:,i-1)') * c(:,i-1,j);
            
            error = abs(pdist2(c_proj', P_proj', 'euclidean') - r(j));
            inliers_count(k) = length(error(error < inliers_threshold)); %compute number of inliers from error
            
            if inliers_count(k) > inliers_req %break if reached desired number of inliers
                flag = 1;
                break;
            end
            k = k + 1;
            
            %search visualization for last 10 points
            if i > length(P)-10
                % plot cylinder
                nn = (eye(3) - ax(:,i-1) * ax(:,i-1)') * [1;0;0]; % get an arbitrary vector orthogonal to axis
                nn = nn / norm(nn);
                R = [cross(nn,ax(:,i-1)) nn ax(:,i-1)]; % create rotation matrix
                [X,Y,Z] = cylinder;
                Z=Z*5 - 2.5; % lengthen cylinder
                m = size(X,1)*size(X,2);
                rotXYZ = R * ([reshape(X,1,m); reshape(Y,1,m); reshape(Z,1,m)]*r(j)) + repmat(c(:,i-1,j),1,m); % rotate cylinder and add offset
                h = surf(reshape(rotXYZ(1,:),2,m/2),reshape(rotXYZ(2,:),2,m/2),reshape(rotXYZ(3,:),2,m/2)); % plot it
                pause(0.000001)
                delete(h)
            end
            
        end
        if flag break;
        end
    end
    [max_inliers, idx_i] = max(inliers_count);
    idx = ceil(idx_i/r_num);
    idx_r = mod(idx_i,r_num);
    if idx_r == 0 idx_r = r_num;
    end
    center = c(:,idx, idx_r);
    radius = r(idx_r);
    axis = ax(:,idx);
end

