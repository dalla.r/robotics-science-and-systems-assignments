% Fit a plane using RANSAC
% input        P ->  3x100 matrix denoting 100 points in 3D space
% output: normal -> 1x3 vector denoting surface normal of the fitting plane
%         center -> 1x3 vector denoting center of the points
function [normal,center] = Q1_c(P)
    iter_limit = 1000; % max number of iterations
    inliers_req = length(P)/3; % number of inliers that causes the loop to break
    inliers_threshold = 0.05; % threshold to consider a point an inlier
    num_samples = 3;
    
    inliers_count = 0; 
    iter_done = 1;
    while iter_done < iter_limit 
        P_shuffle = P(:,randperm(length(P))); %shuffle the points
        P_samples = P_shuffle(:,1:num_samples); %take num_samples number of points

        [normal(:,iter_done), center(:,iter_done)] = Q1_a(P_samples); %compute least squares fit 
        error = abs(dot(P - center(:,iter_done), repmat(normalize(normal(:,iter_done)),1,length(P)))); % distance of each point from plane
        inliers_count(iter_done) = length(error(error < inliers_threshold)); %compute number of inliers from error
        if inliers_count(iter_done) > inliers_req %break if reached desired number of inliers
            break;
        end
        iter_done = iter_done + 1;
    end
    [max_inliers, idx] = max(inliers_count);
    normal = normal(:,idx)';
    center = center(:,idx)';
end
